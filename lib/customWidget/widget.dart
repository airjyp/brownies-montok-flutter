import 'package:flutter/material.dart';

class ListItem extends StatelessWidget {
  final VoidCallback onPressed;
  final String img;
  final String text;
  ListItem({this.onPressed, this.img, this.text});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Card(
        elevation: 2.0,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 10.0,
            ),
            Image.asset(
              img,
              height: 35.0,
            ),
            SizedBox(
              height: 5.0,
            ),
            Text(
              text,
            ),
          ],
        ),
      ),
    );
  }
}

class EditTextReservation extends StatelessWidget {
  final FormFieldSetter<String> onSaved;
  final int maxLines;
  final String errorText;
  final String hintText;
  final TextEditingController controller;
  final TextInputType keyboardType;
  EditTextReservation(
      {this.maxLines,
      this.onSaved,
      this.errorText,
      this.hintText,
      this.controller,
      this.keyboardType});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      controller: controller,
      keyboardType: keyboardType,
      style: TextStyle(
          color: Colors.black, fontSize: 16, fontWeight: FontWeight.w400),
      onSaved: onSaved,
      validator: (e) {
        if (e.isEmpty) {
          return errorText;
        }
      },
      maxLines: maxLines,
      decoration: InputDecoration(
        hintText: hintText,
      ),
    );
  }
}

class TglDropDown extends StatelessWidget {
  TglDropDown(
      {Key key,
      this.child,
      this.labelText,
      this.valueText,
      this.valueStyle,
      this.onPressed})
      : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: onPressed,
      child: new InputDecorator(
        decoration: new InputDecoration(
          labelText: labelText,
        ),
        baseStyle: valueStyle,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text(valueText, style: valueStyle),
            new Icon(Icons.arrow_drop_down,
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.grey.shade700
                    : Colors.white70),
          ],
        ),
      ),
    );
  }
}
