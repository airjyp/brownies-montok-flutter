import 'package:flutter/material.dart';

class BagianInformasi extends StatefulWidget {
  _BagianInformasiState createState() => _BagianInformasiState();
}

class _BagianInformasiState extends State<BagianInformasi> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: true,
          title:
              Text('INFORMATION', style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
          leading: FlatButton(
            child: Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          bottom: TabBar(
            labelColor: Colors.black,
            labelPadding: EdgeInsets.all(8.0),
            indicatorColor: Colors.black,
            tabs: <Widget>[
              Tab(
                text: 'How To Order',
              ),
              Tab(
                text: 'Payment',
              ),
              Tab(
                text: 'Contact Us',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            CaraOrder(),
            Pembayaran(),
            HubungiKami(),
          ],
        ),
      ),
    );
  }
}

class CaraOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(
            'Bagaimana cara order di Brownies Montok',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20.0,
          ),
          Text('❋ Pilih barang yang akan dibeli sesuai dengan keinginan Anda.'),
          SizedBox(
            height: 5.0,
          ),
          Text(
              '❋ Tambahkan ke keranjang belanja, dan periksa kembali pesanan Anda.'),
          SizedBox(
            height: 5.0,
          ),
          Text(
              '❋ Lanjutkan dengan mengisi formulir detail Nama dan Alamat dengan perincian harga total.'),
          SizedBox(
            height: 5.0,
          ),
          Text(
              '❋ Setelah Anda melakukan pemesanan, kami akan segera memeriksa kondisi, ketersediaan dan harga jika ada perubahan harga, serta pengiriman produk yang Anda pesan sendiri, oleh karena itu JANGAN mengirim / mentransfer uang sebelum ada konfirmasi dari kami melalui telepon / sms / email.'),
          SizedBox(
            height: 5.0,
          ),
          Text(
              '❋ Setelah mendapat konfirmasi dari kami, silakan kirim / transfer pembayaran ke salah satu rekening bank kami.'),
          SizedBox(
            height: 5.0,
          ),
          Text(
              '❋ Kami hanya menerima pembayaran tunai melalui transfer ke rekening bank.')
        ],
      ),
    );
  }
}

class Pembayaran extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(
            'Kami akan mengirimkan informasi tentang jumlah total pengeluaran dan ongkos kirim ke pesan Whatsapp atau alamat email Anda, untuk detailnya silakan cek email atau whatsapp Anda!',
          ),
          SizedBox(
            height: 20.0,
          ),
          Text(
            'Pembayaran dapat ditransfer ke :',
          ),
          SizedBox(
            height: 16.0,
          ),
          Column(
            children: <Widget>[
              Text(
                'Bank BCA',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('No. Rekening : '),
                    Text(
                      '12345678',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Atas Nama : '),
                    Text(
                      'Dedy Sujana',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Column(
            children: <Widget>[
              Text(
                'Bank Mandiri',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('No. Rekening : '),
                    Text(
                      '12345678',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Atas Nama : '),
                    Text(
                      'Dedy Sujana',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Column(
            children: <Widget>[
              Text(
                'Bank BNI',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('No. Rekening : '),
                    Text(
                      '12345678',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Atas Nama : '),
                    Text(
                      'Dedy Sujana',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 20.0,
          ),
          Column(
            children: <Widget>[
              Text(
                'Bank BRI',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 5.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('No. Rekening : '),
                    Text(
                      '12345678',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Atas Nama : '),
                    Text(
                      'Dedy Sujana',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Text(
            'Catatan Penting',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
              '❋ Transfer antar rekening BCA, Mandiri, BNI dan BRI, adalah cara mengirim uang dengan cepat, sehingga kami bisa konfirmasi segera.'),
          SizedBox(
            height: 10.0,
          ),
          Text('❋ Setoran tunai, biasanya dapat diterima pada hari yang sama'),
          SizedBox(
            height: 10.0,
          ),
          Text(
              '❋ Transfer antar bank, dapat diterima pada hari yang sama, bisa 1-2 hari setelah transfer dilakukan, terutama jika dilakukan pada sabtu dan minggu'),
          SizedBox(
            height: 20.0,
          ),
          Center(
            child: Text(
              'Kami akan memberikan konfirmasi setelah uang masuk ke rekening bank kami',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }
}

class HubungiKami extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Text(
            'Apakah Anda masih merasa bingung dengan sistem yang Anda butuhkan? Jangan khawatir, silahkan hubungi kami sekarang juga!\n\nDengan senang hati kami akan membantu menyelesaikan kebutuhan Anda.',
            textAlign: TextAlign.justify,
          ),
          SizedBox(
            height: 20.0,
          ),
          Divider(),
          ListTile(
            title: Text(
              'Brownies Montok',
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          ListTile(
            title: Text(
              'Alamat : ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
                'Jalan Brownies Montok Gg. Pisang Coklat\nRt. 10 Rw. 10 No. 100\nSudimara Pinang Ciledug\nTangerang, Banten 15145'),
          ),
          ListTile(
            title: Text(
              'Email : ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text('BrowniesMontok@kuekoe.com'),
          ),
          ListTile(
            title: Text(
              'No. Telpon : ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text('081519104158 (call only)\n081500112233 (WA)'),
          ),
        ],
      ),
    );
  }
}
