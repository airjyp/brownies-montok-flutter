import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/customWidget/widget.dart';
import 'package:ketixclone/models/order.dart';
import 'package:ketixclone/view/checkout/successOrder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ReservationOrders extends StatefulWidget {
  _ReservationOrdersState createState() => _ReservationOrdersState();
}

class _ReservationOrdersState extends State<ReservationOrders> {
  String nama,
      alamat,
      kota,
      propinsi,
      username,
      idCustomer,
      tanggalKirim,
      telpon,
      komen,
      labelText;
  TextEditingController textNama,
      textAlamat,
      textKota,
      textPropinsi,
      textTelpon,
      textEmail;

  _textController() {
    textNama = TextEditingController(text: nama);
    textAlamat = TextEditingController(text: alamat);
    textKota = TextEditingController(text: kota);
    textPropinsi = TextEditingController(text: propinsi);
    textTelpon = TextEditingController(text: telpon);
    textEmail = TextEditingController(text: username);
  }

  final _key = new GlobalKey<FormState>();
  final TextStyle valueStyle = TextStyle(fontSize: 18.0);
  final semuaPesanan = List<TotalTransaction>();
  final totalOrders = List<TotalOrders>();
  final oCcy = new NumberFormat("#,##0", "en_US");
  var total;
  var cek = false;
  var isLoading = false;

  DateTime date = new DateTime.now();
  Future<Null> _selectedData(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: date,
        firstDate: DateTime(1800),
        lastDate: DateTime(2025));

    if (picked != null && picked != date) {
      setState(() {
        date = picked;
        tanggalKirim = new DateFormat("y-M-d").format(date);
      });
    }
  }

  _submit() {
    final formulir = _key.currentState;
    if (formulir.validate()) {
      formulir.save();
      _reservation();
      _hasSuccess();
      _finishTransaksi();
      cek ? _showToast() : SizedBox();
    }
  }

  _savePreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString('nama', nama);
      preferences.setString('alamat', alamat);
      preferences.setString('kota', kota);
      preferences.setString('propinsi', propinsi);
      preferences.setString('telpon', telpon);
      preferences.commit();
    });
  }

  _reservation() async {
    setState(() {
      isLoading = true;
    });
    final responses = await http.post(ApiUrl.baseUrl + ApiUrl.sendTo, body: {
      'name': textNama.text,
      'alamat': textAlamat.text,
      'kota': textKota.text,
      'provinsi': textPropinsi.text,
      'name2': _pilihKurir,
      'date_n_time': date.toString(),
      'phone': textTelpon.text,
      'comment': komen,
      'username': username,
    });

    final sendData = jsonDecode(responses.body);
    int value = sendData['value'];
    String msg = sendData['message'];
    if (value == 1) {
      setState(() {
        cek = true;
        isLoading = false;
      });
      //_warningDialog(msg);
    }
  }

  _finishTransaksi() async {
    final responses = await http.post(ApiUrl.baseUrl + ApiUrl.finish, body: {
      'username': username,
      'idCustomer': idCustomer,
    });
    final getData = jsonDecode(responses.body);
    int value = getData['value'];
    String msg = getData['message'];
    if (value == 1) {
      setState(() {
        print('$msg');
        _savePreferences();
      });
    } else {
      var dialog = AlertDialog(
        title: Text('Warning'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('$msg'),
            ],
          ),
        ),
      );
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return dialog;
          });
    }
  }
  
  _getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString('username');
      idCustomer = preferences.getString('idCustomer');
      nama = preferences.getString('nama');
      kota = preferences.getString('kota');
      telpon = preferences.getString('telpon');
      nama = preferences.getString('nama');
    });
    _getAllOrder();
    _totalHargaOrder();
    _textController();
    print('$nama');
    print('$kota');
    print('$telpon');
  }

  _getAllOrder() async {
    semuaPesanan.clear();
    setState(() {
      isLoading = true;
    });
    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.allTmpTransaksi + username);

    if (responses.contentLength == 2) {
      setState(() {
        cek = false;
      });
    } else {
      final getData = jsonDecode(responses.body);
      getData.forEach((api) {
        final all = new TotalTransaction(
            api['id'],
            api['Menu_ID'],
            api['Menu_name'],
            api['Quantity'],
            api['Price'],
            api['Weight'],
            api['Tmp_image'],
            api['idCustomer'],
            api['username'],
            api['total']);
        semuaPesanan.add(all);
      });
      setState(() {
        cek = true;
      });
    }
  }

  _totalHargaOrder() async {
    totalOrders.clear();
    setState(() {
      isLoading = true;
    });
    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.sumTmpTransaksi + username);
    final getData = jsonDecode(responses.body);
    getData.forEach((api) {
      final total = new TotalOrders(api['total']);
      totalOrders.add(total);
    });
    setState(() {
      isLoading = false;
    });
  }

  _orderConfirmaton() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => SuksesCheckout()));
  }

  _hasSuccess() {
    new Timer(new Duration(seconds: 3), () {
      _orderConfirmaton();
    });
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
    _menuGoSend = _buildMenuItem(_kurirGoSend);
    _pilihKurir = _menuGoSend[0].value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: FlatButton(
            child: Icon(Icons.arrow_back_ios, color: Colors.black),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            'Checkout Details',
            style: TextStyle(color: Colors.black),
          )),
      body: Form(
        key: _key,
        child: Container(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    EditTextReservation(
                      controller: textNama,
                      onSaved: (e) => nama = (e),
                      keyboardType: TextInputType.text,
                      hintText: 'Nama lengkap',
                      errorText: 'Nama tidak boleh kosong',
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    EditTextReservation(
                      controller: textAlamat,
                      onSaved: (e) => alamat = e,
                      keyboardType: TextInputType.multiline,
                      hintText: 'Alamat lengkap',
                      errorText: 'Alamat tidak boleh kosong',
                      maxLines: 3,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    EditTextReservation(
                      controller: textKota,
                      onSaved: (e) => kota = e,
                      keyboardType: TextInputType.text,
                      hintText: 'Kota / Kecamatan',
                      errorText: 'Kota tidak boleh kosong',
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    EditTextReservation(
                      controller: textPropinsi,
                      onSaved: (e) => propinsi = e,
                      keyboardType: TextInputType.text,
                      hintText: 'Provinsi / Kabupaten',
                      errorText: 'Provinsi tidak boleh kosong',
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  '+62',
                                  style: TextStyle(fontSize: 20),
                                ),
                              ],
                            )),
                        Expanded(
                          flex: 8,
                          child: EditTextReservation(
                            controller: textTelpon,
                            onSaved: (e) => telpon = e,
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            hintText: 'No. Telpon',
                            errorText: 'Telpon tidak boleh kosong',
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Jasa Pengiriman : '),
                        DropdownButton(
                          value: _pilihKurir,
                          items: _menuGoSend,
                          onChanged: (e) {
                            setState(() {
                              _pilihKurir = e;
                            });
                          },
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(flex: 3, child: Text('Kapan Dikirim : ')),
                        Expanded(
                          flex: 2,
                          child: Center(
                            child: TglDropDown(
                              labelText: labelText,
                              valueText:
                                  new DateFormat('dd-MM-yyyy').format(date),
                              valueStyle: valueStyle,
                              onPressed: () {
                                _selectedData(context);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    cek ? _buildSemuaPesanan() : SizedBox(),
                    SizedBox(
                      height: 10.0,
                    ),
                    cek ? _buildTotalPesanan() : _noAddtoCart(),
                    SizedBox(
                      height: 10.0,
                    ),
                    EditTextReservation(
                      onSaved: (e) => komen = e,
                      keyboardType: TextInputType.text,
                      hintText: 'Pesan',
                      errorText: 'Pesan tidak boleh kosong',
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    TextFormField(
                      controller: textEmail,
                      onSaved: (e) => username = e,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w400),
                      validator: (value) {
                        if (value.contains('@')) {
                          return null;
                        } else {
                          return 'Mohon masukkan email dengan benar';
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
          child: Material(
        color: Colors.brown,
        child: MaterialButton(
          onPressed: () {
            if (cek == true) {
              _submit();
            } else {
              _orderDuluGaes();
            }
          },
          child: Text(
            'Submit',
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
      )),
    );
  }

  _showToast() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(8.0),
                  color: Colors.black,
                  child: Text(
                    'Please wait...',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                    padding: EdgeInsets.all(8.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    )),
              ],
            ),
          );
        });
  }

  Future<bool> _orderDuluGaes() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16.0),
                      color: Colors.black,
                      child: Text(
                        'Konfirmasi',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      child: Text('Pilih produk terlebih dahulu.'),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: SizedBox(),
                          ),
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop(true);
                            },
                            child: Text('OK'),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ) ??
              false;
        });
  }

  String orderan;
  Widget _buildSemuaPesanan() {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: semuaPesanan.length,
        itemBuilder: (context, index) {
          final sp = semuaPesanan[index];
          orderan = sp.quantity + ' ' + sp.menuName;
          return ListView(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text('$orderan',
                      style: TextStyle(
                          color: Colors.brown,
                          fontSize: 16.0,
                          fontWeight: FontWeight.w300)),
                ],
              ),
            ],
          );
        },
      ),
    );
  }

  Widget _noAddtoCart() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Text('Produk tidak ditemukan'),
      ),
    );
  }

  Widget _buildTotalPesanan() {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: totalOrders.length,
        itemBuilder: (context, index) {
          final to = totalOrders[index];
          return Row(children: <Widget>[
            Text('Total',
                style: TextStyle(
                    color: Colors.brown,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w300)),
            SizedBox(
              width: 5.0,
            ),
            Text(
              'Rp. ' + oCcy.format(int.parse(to.total)) + '.-',
              style: TextStyle(
                  color: Colors.brown,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w300),
            )
          ]);
        },
      ),
    );
  }
}

List _kurirGoSend = ['GO-JEK', 'GRAB', 'JNE'];
String _pilihKurir;
List<DropdownMenuItem<String>> _menuGoSend;

_buildMenuItem(List kurirGoSend) {
  List<DropdownMenuItem<String>> goSend = new List();
  for (String goSends in kurirGoSend) {
    goSend.add(new DropdownMenuItem(
      value: goSends,
      child: Text(goSends),
    ));
  }
  return goSend;
}
