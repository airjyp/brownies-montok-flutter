
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:ketixclone/api.dart';
import 'package:intl/intl.dart';
import 'package:ketixclone/models/menu.dart';
import 'package:ketixclone/view/cart/allCart.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ProductDetails extends StatefulWidget {
  final MenuItems items;
  ProductDetails(this.items);
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String totalOrder;
  int angka = 0;
  final oCcy = new NumberFormat("#,##0", "en_US");
  ScrollController _hideButtonController;
  var _isVisible;
  final _key = new GlobalKey<FormState>();

  void showInSnackBar() {
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text('Pesanan berhasil ditambahkan ke keranjang')));
  }

  _kurangiTmpProduk(String idMenu) async {
    await http.post(ApiUrl.baseUrl + ApiUrl.deleteTmpProduk,
        body: {'username': username, 'Menu_ID': idMenu});
    setState(() {
    });
  }

  _tambahTmpProduk(String angka, String idMenu, String price, String weight,
      String tmpImage) async {
    await http.post(ApiUrl.baseUrl + ApiUrl.addTmpProduk, body: {
      'idCustomer': idCustomer,
      'username': username,
      'qty': angka,
      'Menu_ID': idMenu,
      'price': price,
      'weight': weight,
      'Tmp_image': tmpImage
    });
    setState(() {
    });
  }

  String idCustomer, username;
  _getPreference() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idCustomer = preferences.getString('idCustomer');
      username = preferences.getString('username');
      print('$idCustomer');
      print('$username');
    });
  }

  @override
  void initState() {
    super.initState();
    _getPreference();
    _isVisible = false;
    _hideButtonController = new ScrollController();
    _hideButtonController.addListener(() {
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        setState(() {
          _isVisible = true;
        });
      }
      if (_hideButtonController.position.userScrollDirection ==
          ScrollDirection.forward) {
        setState(() {
          _isVisible = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        title: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.asset('./img/brownies_logo.png'),
        ),
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          new Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Container(
                height: 150.0,
                width: 30.0,
                child: new GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AllCartProduct()));
                  },
                  child: new Stack(
                    children: <Widget>[
                      Image.asset(
                        './img/toko_buku.png',
                        height: 30.0,
                        width: 30.0,
                      ),
                      angka == 0
                          ? new Container()
                          : new Positioned(
                              child: new Stack(
                              children: <Widget>[
                                new Icon(Icons.brightness_1,
                                    size: 20.0, color: Colors.red),
                                new Positioned(
                                    top: 2.0,
                                    right: 4.0,
                                    child: new Center(
                                      child: new Text(
                                        angka.toString(),
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )),
                              ],
                            )),
                    ],
                  ),
                )),
          )
        ],
      ),
      body: CustomScrollView(
        shrinkWrap: true,
        controller: _hideButtonController,
        slivers: <Widget>[
          SliverPadding(
            padding: EdgeInsets.all(0.0),
            sliver: SliverList(
              delegate: SliverChildListDelegate(<Widget>[
                Container(
                  //padding: EdgeInsets.all(16.0),
                  child: ListView(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    children: <Widget>[
                      Container(
                        height: 200.0,
                        width: double.infinity,
                        child: Hero(
                            tag: widget.items.idMenu,
                            child: Image.network(
                              ApiUrl.baseUrl + widget.items.imageMenu,
                              fit: BoxFit.cover,
                            )),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: ListView(
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          children: <Widget>[
                            Text(
                              widget.items.nameMenu,
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Divider(
                              color: Colors.grey[300],
                              height: 20.0,
                            ),
                            Text(
                              'Tersedia : ' + widget.items.quantity + ' pcs',
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                            Divider(
                              color: Colors.grey[300],
                              height: 20.0,
                            ),
                            Text(
                              'Status : ' + widget.items.stock,
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                            Divider(
                              color: Colors.grey[300],
                              height: 20.0,
                            ),
                            Text(
                              'Berat : ' + widget.items.weight + ' Kg',
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                            Divider(
                              color: Colors.grey[300],
                              height: 20.0,
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 20.0, bottom: 8.0),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "Description",
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Expanded(
                                    child: new Container(
                                      margin: EdgeInsets.all(10.0),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 0.25,
                                              color: Colors.black)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Text(
                              widget.items.description,
                              style: TextStyle(fontWeight: FontWeight.w300),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ),
        ],
      ),
      floatingActionButton: _isVisible == true
          ? SizedBox()
          : Material(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(50.0),
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Text(
                        'Rp. ' +
                            oCcy.format(int.parse(widget.items.price)) +
                            '.-',
                        style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    FloatingActionButton.extended(
                      tooltip: 'Add to Cart',
                      backgroundColor: Colors.brown,
                      icon: Icon(Icons.check),
                      label: Text('Add to Cart'),
                      onPressed: _totalOrder,
                    ),
                  ],
                ),
              ),
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  int total = 0;
  Widget _angkaPesanan() {
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          //mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              'QTY',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Row(
              //mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                angka == 0
                    ? OutlineButton(
                        padding: EdgeInsets.all(5.0),
                        onPressed: () {},
                        child: Text(
                          '-',
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    : OutlineButton(
                        padding: EdgeInsets.all(5.0),
                        onPressed: () {
                          setState(() {
                            angka--;
                            total = int.parse(widget.items.price) * angka;
                          });
                        },
                        child: Text(
                          '-',
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                SizedBox(
                  width: 5.0,
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey[300])),
                  child: Text(
                    '$angka',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                OutlineButton(
                  onPressed: () {
                    setState(() {
                      angka++;
                      total = int.parse(widget.items.price) * angka;
                    });
                  },
                  child: Text(
                    '+',
                    style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            ),
            Row(
              children: <Widget>[
                Text(
                  'Sub Total : ',
                  style: TextStyle(fontSize: 16.0),
                ),
                Text(
                  'Rp. ' + oCcy.format(int.parse('$total')) + ',-',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            ),
          ],
        );
      },
    );
  }

  _totalOrder() {
    showModalBottomSheet(
      context: context,
      builder: (builder) {
        return Form(
          key: _key,
          child: Container(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 100.0,
                        width: 150.0,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image: NetworkImage(
                                    ApiUrl.baseUrl + widget.items.imageMenu))),
                      ),
                    ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.items.nameMenu,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 50.0,
                          ),
                          Text('@Rp. ' +
                              oCcy.format(int.parse(widget.items.price)) +
                              ',-')
                        ])
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _angkaPesanan(),
                ),
                Padding(
                  padding: EdgeInsets.all(0.0),
                  child: Material(
                    color: Colors.brown,
                    //borderRadius: BorderRadius.circular(30.0),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                        setState(() {
                          if (angka == 0) {
                            _kurangiTmpProduk(widget.items.idMenu);
                          } else {
                            _tambahTmpProduk(
                                angka.toString(),
                                widget.items.idMenu,
                                widget.items.price,
                                widget.items.weight,
                                widget.items.imageMenu);
                            showInSnackBar();
                          }
                        });
                      },
                      child: Text(
                        'Tambahkan',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
