import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:ketixclone/api.dart';
import 'package:ketixclone/models/category.dart';
import 'package:http/http.dart' as http;
import 'package:ketixclone/view/cart/allCart.dart';
import 'package:ketixclone/view/product/allProduct.dart';

class CategoryProduct extends StatefulWidget {
  _CategoryProductState createState() => _CategoryProductState();
}

class _CategoryProductState extends State<CategoryProduct> {
  final categoryItem = List<CategoryItems>();
  bool isLoading = false;

  _fetchCategoryItems() async {
    categoryItem.clear();
    setState(() {
      isLoading = true;
    });

    final responses =
        await http.get(ApiUrl.baseUrl + ApiUrl.categoryItem + ApiUrl.accessKey);
    final getData = jsonDecode(responses.body);
    getData.forEach((api) {
      final brow = new CategoryItems(
          api['Category_ID'], api['Category_name'], api['Category_image']);
      categoryItem.add(brow);
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _fetchCategoryItems();
    isLoading = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title:
            Text('All Product Category', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        leading: FlatButton(
          child: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => AllCartProduct()));
            },
            child: Padding(
              padding: const EdgeInsets.all(14.0),
              child: Image.asset(
                './img/toko_buku.png',
                height: 30.0,
                width: 30.0,
              ),
            ),
          )
        ],
      ),
      body: _loadCategoryItems(),
    );
  }

  Widget _loadCategoryItems() {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return GridView.builder(
          primary: true,
          itemCount: categoryItem.length,
          gridDelegate:
              new SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 1.1,
                crossAxisCount: 2),
          itemBuilder: (context, index) {
            final listCategory = categoryItem[index];
            return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ProductByCategory(listCategory)));
                },
                child: Card(
                  elevation: 3.0,
                  child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: <Widget>[
                      FadeInImage.assetNetwork(
                        placeholder: './img/loading_icon.gif',
                        image: ApiUrl.baseUrl + listCategory.imagecategory,
                        fit: BoxFit.fill,
                        height: 125.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          '${listCategory.nameCategory}',
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ));
          });
    }
  }
}

class CategoryLists extends StatefulWidget {
  final CategoryItems categoryList;
  CategoryLists({this.categoryList});
  _CategoryListsState createState() => _CategoryListsState();
}

class _CategoryListsState extends State<CategoryLists> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 2.0,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8.0),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ProductByCategory(widget.categoryList)));
                  },
                  child: Column(
                    children: <Widget>[
                      FadeInImage.assetNetwork(
                        placeholder: './img/loading_icon.gif',
                        image:
                            ApiUrl.baseUrl + widget.categoryList.imagecategory,
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        '${widget.categoryList.nameCategory}',
                        textAlign: TextAlign.center,
                      )
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
